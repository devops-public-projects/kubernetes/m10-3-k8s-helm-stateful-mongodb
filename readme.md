
# Kubernetes Helm | Stateful MongoDB Service
Install a stateful service (MongoDB) on Kubernetes using Helm.

## Technologies Used
- Kubernetes
- Helm
- MongoDB
- Mongo-Express
- Linode LKE
- Linux

## Project Description
- Create a managed K8s cluster with Linode Kubernetes Engine (LKE)
- Deploy replicated MongoDB service in LKE cluster using a Helm chart
- Configure data persistence for MongoDB with Linode’s cloud storage
- Deploy UI client Mongo Express for MongoDB
- Deploy and configure nginx Ingress to access the UI application from browser

## Prerequisites
- [Helm Installation](https://helm.sh/docs/intro/install)
- [Starting Code](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/helm-demo)
- [Linode Account](https://www.linode.com/)

## Guide Steps
### Create K8s Cluster
- Login to cloud.linode.com and select from the **Create** drop-down **Kubernetes**
- Create Cluster Settings
	- Cluster Label: **Test**
	- Region: **Closest to you**
	- Kubernetes Version: **Latest**
	- HA Control Plane: **No**
	- Node Pool: **2x Linode 4 GB**
	- **Create Cluster**
- Download the `test-kubeconfig.yaml` from the cluster portal
- Set `test-kubeconfig.yaml` as a environment variable to access your cluster
	- `chmod 400 test-kubeconfig.yaml`
	- `export KUBECONFIG=test-kubeconfig.yaml`
	- Verify you can see the **two** Linode nodes
		- `kubectl get node`
### Configure Helm
- Add repository for helm
	- `helm repo add bitnami https://charts.bitnami.com/bitnami`
	- Find the MongoDB repo
		- `helm search repo bitnami/mongodb`
	- Inside of our `helm-mongodb.yaml` we have overridden some basic values from the repo defaults
	- architecture: **replicaset**
	- replicaCount: **3**
	- storageClass: **"linode-block-storage"**
	- rootPassword: **db_admin_password**

### Install Helm Chart
- `helm install mongodb --values helm-mongodb.yaml bitnami/mongodb`

![Successful MongoDB Helm Install](/images/m10-3-successful-mongodb-helm-install.png)

![MongoDB Pods Spinning Up](/images/m10-3-mongodb-pods.png)

### Install Mongo Express
- All configuration is inside `helm-mongo-express.yaml`, run it to install
	- `kubectl apply -f helm-mongo-express.yaml`
- Verify success
	- `kubectl get pod`

![Mongo Express Running](/images/m10-3-mongo-express-running.png)

### Configure K8s Ingress
- Add Helm Repo for nginx Ingress
	- `helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx`
- Install Helm Chart
	- `helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.publishService.enabled=true`

![Successful Nginx Install](/images/m10-3-successful-nginx-install.png)

### Configure Helm Ingress
- All contents are in the `helm-ingress.yaml` file, you just need to modify the **host** attribute to be the **Linode Nodebalancer Host Name**
	- It will look something like this: `172-234-3-93.ip.linodeusercontent.com`
- Run the file to apply
	- `kubectl apply -f helm-ingress.yaml`
- Verify Service
	- `kubectl get service`

![Successful Ingress](/images/m10-3-successful-ingress.png)

- You can now access `Mongo-Express` via the web browser using the host you input into the `helm-ingress.yaml` file